package computeLikelihood;

//
/**
ssh mtschmitz@adhara.biostat.wisc.edu 
 * Bioinformatics HW3
 * Matt Schmitz
 * 10/28/2014
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;
import java.util.concurrent.LinkedBlockingQueue;


public class ComputeLikelihood {
  private static  Tree tree = new Tree();
  public static Double[][] ScoreMatrix = new Double[4][4];
  public static String[] alphabet = new String[4];
  public static Double[][] updateMatrix;
  
  public static void main(String[] args) {
    // Make sure an input file was specified on the command line.
    // If this fails and you are using Eclipse, see the comments 
    // in the header above.
    if (args.length != 3 ) {
      System.err.println("Please supply a filename on the " +
			 "command line: java computeLikelihood ");
      System.exit(1);
    }
    
    // Try creating a scanner to read the input file.
    Scanner treeFileScanner = null;
    try {
      treeFileScanner = new Scanner(new File(args[0]));
    } catch(FileNotFoundException e) {
      System.err.println("Could not find file '" + args[0] + 
			 "'.");
      System.exit(1);
    }
    
    Scanner scoreFileScanner = null;
    try {
      scoreFileScanner = new Scanner(new File(args[1]));
    } catch(FileNotFoundException e) {
      System.err.println("Could not find file '" + args[1] + 
			 "'.");
      System.exit(1);
    }
    
    Scanner assignFileScanner = null;
    try {
      assignFileScanner = new Scanner(new File(args[2]));
    } catch(FileNotFoundException e) {
      System.err.println("Could not find file '" + args[2] + 
			 "'.");
      System.exit(1);
    }
    
    
    // Create a DatasetParser, parse the datasets
    DatasetParser parser = new DatasetParser();
    //Build the tree from the files
    parser.parseTreeFile(treeFileScanner);   
    parser.parseScoreFile(scoreFileScanner);
    parser.parseAssignFile(assignFileScanner);
    Float prob = (float) computeLikelihood(ComputeLikelihood.tree).doubleValue();
 //   printTree();
    System.out.println("Likelihood of tree is " + prob);
  }
  
  
  /*
   * Public method to compute the cost of a tree. uses helper
   * method to do recursion
   */
  
  public static Double computeLikelihood(Tree tree){
  	Node root = tree.root();
  	Double cost = 0.0;
  	helpComputeLikelihood(root);
//  	System.out.println(ComputeLikelihood.updateMatrix[root.index-2][1]);
  	for(int i = 0; i<ComputeLikelihood.alphabet.length; i++){
  		cost = cost + ComputeLikelihood.updateMatrix[root.index][i];
  	}
  	return .25*cost;
  }
  
  
  private static void helpComputeLikelihood( Node n) {
		if(n.children.isEmpty()){
		}else{
			//
			helpComputeLikelihood(n.children.get(0));
			helpComputeLikelihood(n.children.get(1));
			//Fill in the updating Likelihood matrix
			sum(n , n.children.get(0),n.children.get(1));
		}
	}

  
  /*
   *   You have parent and Child nodes
   *   min number initialized 0.0
   *   find the minimum cost from child to parent
   *   The minimum cost leads to the setting of the parent to that value
   *   
   */

private static void sum(Node parent, Node child1, Node child2) {
	
	// Find indexes of the values of children
	for(int i = 0; i<ComputeLikelihood.alphabet.length; i++){
		if(child1.value != null){
			if(child1.value.equalsIgnoreCase(alphabet[i])){
				ComputeLikelihood.updateMatrix[child1.index][i]=1.0;
			}else{
				ComputeLikelihood.updateMatrix[child1.index][i]=0.0;
			}
		}
		if(child2.value != null){
			if(child2.value.equalsIgnoreCase(alphabet[i])){
				ComputeLikelihood.updateMatrix[child2.index][i]=1.0;
			}else{
				ComputeLikelihood.updateMatrix[child2.index][i]=0.0;
			}
			}
		}

	
//	Sum the probability for each set of possible values
	for(int i = 0; i<ComputeLikelihood.alphabet.length; i++){
		Double sum1 = 0.0;	
		Double sum2 = 0.0;
		Double sum = 0.0;
		for( int j = 0; j<ComputeLikelihood.alphabet.length; j++){
			sum1 = sum1 + ComputeLikelihood.updateMatrix[child1.index][j] * ComputeLikelihood.ScoreMatrix[i][j];
			sum2 =  sum2 + ComputeLikelihood.updateMatrix[child2.index][j]* ComputeLikelihood.ScoreMatrix[i][j];
			//System.out.println(ComputeLikelihood.updateMatrix[child1.index][j] + " " + ComputeLikelihood.ScoreMatrix[i][j] + " sum1=" + sum1 + " sum2=" + sum2);
		}sum = sum + sum1 *sum2;
		//Update the probability matrix
		ComputeLikelihood.updateMatrix[parent.index][i] = sum;		
	}	
}


public static void printTree(){
	Node node = getTree().root();
	printTree( node);
}
/*
 * Recursive method which prints the branches of the tree
 * with the number of (+) examples, and the number of (-) examples
 */
private static void printTree( Node node){
	System.out.println(node.index);
	if(!node.children.isEmpty()){
		printTree(node.children.get(0));
		printTree(node.children.get(1));
	}

}


private Stack<Node> orderNodes() throws InterruptedException {
  	Node root = ComputeLikelihood.getTree().root();
	LinkedBlockingQueue<Node> q = new LinkedBlockingQueue<Node>();
	Stack<Node> stack = new Stack<Node>();
	q.put(root);
	while(!q.isEmpty()){
		Node curr = q.poll();
		stack.push(curr);
		for(Node child : curr.children){
			if(child != null){
				q.put(child);
			}
		}
	}
	return stack;
}






public static Tree getTree() {
	return tree;
	}

	
public static void setTree(Tree tree) {
	ComputeLikelihood.tree = tree;
	}
  
}//end class


class Tree {
    HashMap<Integer, Node> nodes = new HashMap<Integer, Node>();
    public Node addNode(int i ){
    	Node n = new Node(i);
    	 this.nodes.put(i, n);
    	 return n;
    }
    public Node root(){
    	for (Node node : this.nodes.values()) {
    		if (node.parents.isEmpty() == true) 
    			return node;
    		}
    	return null;
    	}
}

class Node { 
    ArrayList<Node> children = new ArrayList<Node>();
    ArrayList<Node> parents = new ArrayList<Node>();
    int index;
    String value;
    Node(int i) { 
      index = i; 
    } 
    public Node getParent(){
    	return this.parents.get(0);
    }
  }

/**
 * This class is responsible for parsing datasets in specific format.
 * Builds a tree from the tree file, puts values into the tree with the
 * assign file and creates the score matrix with the score file.
 */
class DatasetParser {
  public DatasetParser() {}
  
  public void parseAssignFile(Scanner assignFileScanner) {
	  while(assignFileScanner.hasNextLine()) {
	      String line = assignFileScanner.nextLine().trim();
		  if(line == null) continue;
	      String[] l = line.split("\t");
	      for(int i =0; i< l.length; i++){
	    	  String[] s = l[i].split("=");
		      Node node = ComputeLikelihood.getTree().nodes.get(Integer.parseInt(s[0].split("S")[1]));
		      node.value = s[1];
	      }
	  }
}

public void parseScoreFile(Scanner scoreFileScanner) {
	 // String firstline = scoreFileScanner.nextLine();
	
	  String firstline = findSignificantLine(scoreFileScanner);
	  String[] z = firstline.split("\t");
	  for(int i=0; i<z.length; i++){
		  ComputeLikelihood.alphabet[i] = z[i];
	  }
	  
	  int j = 0;
	  while(scoreFileScanner.hasNextLine()) {
		  String line = findSignificantLine(scoreFileScanner);
		  if(line == null) continue;
	      String[] l = line.split("\t");
	      for(int i=0; i< 4; i++){
	    	  ComputeLikelihood.ScoreMatrix[i][j] = Double.parseDouble(l[i+1]);
	      }
	      j++;
	  }	
}



public void parseTreeFile(Scanner treeFileScanner) {
	int numNodes = 2;  
	while(treeFileScanner.hasNextLine()) {
	      String line = findSignificantLine(treeFileScanner).trim();
		  if(line == null) continue;
	      String[] l = line.split("\t");
	      //System.out.println(l[0].split("S")[1]);
	      if(ComputeLikelihood.getTree().nodes.get(Integer.parseInt(l[0].split("S")[1]))== null){
	    	  ComputeLikelihood.getTree().addNode(Integer.parseInt(l[0].split("S")[1]));
	      }
	      //Check if parent or child are not in the tree
	      Node child = ComputeLikelihood.getTree().nodes.get(Integer.parseInt(l[0].split("")[2]));
	      Node parent = ComputeLikelihood.getTree().nodes.get(Integer.parseInt(l[1].split("")[2]));
	      
	      //If not, add them and 
	      if(child == null){
	    	  child = ComputeLikelihood.getTree().addNode(Integer.parseInt(l[0].split("")[2]));
	      }
	      if (parent == null){
	    	  parent = ComputeLikelihood.getTree().addNode(Integer.parseInt(l[1].split("")[2]));
	      }
	      numNodes++ ;
	      //link parent and child to each other
//	      System.out.println(Integer.parseInt(l[0].split("")[2]));
//	      System.out.println(Integer.parseInt(l[1].split("")[2]));
	      ComputeLikelihood.getTree().nodes.get(Integer.parseInt(l[0].split("")[2])).parents.add(parent);
	      parent.children.add(child);
//	      System.out.println(child.index);
	   //   System.out.println(numNodes);
	  }

	ComputeLikelihood.updateMatrix = new Double[numNodes][ComputeLikelihood.alphabet.length];
	  
}



/**
   * Returns the first token encountered on a significant line in the file.
   *
   * @param fileScanner a Scanner used to read the file.
   */
  private String parseSingleToken(Scanner fileScanner) {
    String line = findSignificantLine(fileScanner);

    // Once we find a significant line, parse the first token on the
    // line and return it.
    Scanner lineScanner = new Scanner(line);
    return lineScanner.next();
  }


  /**
   * Returns the next line in the file which is significant (i.e., is not
   * all whitespace or a comment).
   *
   * @param fileScanner a Scanner used to read the file
   */
  private String findSignificantLine(Scanner fileScanner) {
    // Keep scanning lines until we find a significant one.
    while(fileScanner.hasNextLine()) {
      String line = fileScanner.nextLine().trim();
      if (isLineSignificant(line)) {
	return line;
      }
    }

    // If the file is in proper format, this should never happen.
    return null;
  }

  /**
   * Returns whether the given line is significant (i.e., not blank or a
   * comment). The line should be trimmed before calling this.
   *
   * @param line the line to check
   */
  private boolean isLineSignificant(String line) {
    line = line.trim(); // Do this again for robustness in case we ever call this from other code.  It is a fast calculation.
    // Blank lines are not significant.
    if(line == null || line.length() == 0) { // Should never get null, but still good idea to check.
      return false;
    }

    // Lines which have consecutive forward slashes as their first two
    // characters are comments and are not significant.
    if(line.startsWith("//")) {
      return false;
    }

    return true;
  }
}